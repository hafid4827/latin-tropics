import React from "react";
/* components */
import { SectionHeroVideo } from "../../components/SectionHeroVideo";
import { SectionMaxi } from "../../components/SectionMaxi";
import { SectionMostMain } from "../../components/SectionMostMain";
import { SectionCATBuy } from "../../components/SectionCATBuy";
import { SectionDetailBuy } from "../../components/SectionDetailBuy";
import { SectionFooterBuy } from "../../components/SectionFooterBuy";
/* imports img */
import ImgHeroHome from "../../static/img/base_img/Recurse_1_100.jpg";
export const HomeLatinTropical = () => {
  return (
    <>
      <header className="">
        <img className="" src={ImgHeroHome} alt="img hero header" />
      </header>
      <section className="">
        <SectionHeroVideo />
      </section>
      <main className="">
        <section className="">
          <SectionMaxi />
        </section>
        <section className="">
          <SectionMostMain />
        </section>
        <section className="">
          <SectionCATBuy />
        </section>
        <section className="mt-16">
          <SectionDetailBuy />
        </section>
      </main>
      <footer className="mt-16">
        <section className="">
          <SectionFooterBuy />
        </section>
      </footer>
    </>
  );
};
