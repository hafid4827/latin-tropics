/* STYLES TAILWIND FRAMEWORK */
import "./static/css/index.css";
/* COMPONETS PACKAGE INSTALLS USE */
import { BrowserRouter, Routes, Route } from "react-router-dom";
/* COMPONENTS PAGE */
import { HomeLatinTropical } from "./pages/Home/HomeLatinTropical";
const App = () => {
  return (
    <div className="App">
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<HomeLatinTropical />} />
          <Route path="/home" element={<HomeLatinTropical />} />
        </Routes>
      </BrowserRouter>
    </div>
  );
};

export default App;
