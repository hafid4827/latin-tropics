import React from "react";
/* mendia img  */
import Recurse3100 from "../static/img/base_img/Recurse_3_100.jpg";
import Recurse4100 from "../static/img/base_img/Recurse_4_100.jpg";
import Recurse5100 from "../static/img/base_img/Recurse_5_100.jpg";
import Recurse6100 from "../static/img/base_img/Recurse_6_100.jpg";
/* components */
import { BtnGenericBuy } from "./Btn/BtnGenericBuy";
export const SectionMostMain = () => {
  return (
    <div className="relative md:h-[95rem] h-auto">
      {/* elements decorations */}
      <div className="absolute md:w-96 w-full md:top-0 top-1 h-96 bg-[color:var(--color-coffee-light-1)]"></div>
      <div className="absolute right-0 md:w-64 w-1/2 bottom-20 h-[70rem] bg-[color:var(--color-coffee-light-1)]"></div>
      <div className="absolute bottom-20 left-0 w-full h-36 bg-[color:var(--color-coffee-light-1)]"></div>
      {/* fin element decorations */}

      {/* messy product container */}
      <div className="top-0 left-0 w-full h-auto md:h-[90rem] md:absolute md:p-0 p-5">
        <div className="md:block flex justify-center flex-col items-center relative w-full h-full">
          <div className="md:mb-0 mb-10 md:absolute xl:w-[30rem] md:w-96 w-56 h-auto xl:-top-28 -top-16 xl:right-40 right-16 md:border-8 border-white">
            <img
              className="w-full md:h-full h-auto"
              src={Recurse3100}
              alt="firl dress fashion orange"
            />
            <div className="relative">
              <div className="md:absolute flex justify-center w-full -bottom-20">
                <BtnGenericBuy value="comprar ahora" />
              </div>
            </div>
          </div>
          <div className="md:absolute w-56 md:w-[30rem] h-auto top-6 left-28 xl:left-40">
            <img
              src={Recurse4100}
              alt="sandals with a background of palm leaves"
            />
          </div>
          <div className="md:absolute w-full h-auto flex items-center md:flex-row flex-col bottom-1 -right-96 xl:-right-[45rem]">
            <div className="xl:-mb-56 md:-mb-16 mb-0 md:mt-0 mt-10">
              <img
                className="md:w-[30rem] w-56 xl:w-[35rem]"
                src={Recurse5100}
                alt="palm leaf print dress"
              />
            </div>
            <div className="md:self-end -mb-2 md:mt-0 mt-10">
              <img
                className="xl:full md:w-full w-56 h-96"
                src={Recurse6100}
                alt="column and palms background"
              />
            </div>
          </div>
        </div>
      </div>
      {/* -------END--------- */}
    </div>
  );
};
