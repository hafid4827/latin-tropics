import React from "react";
/* import media */
import BackgroundHeroSection from "../static/video/BackgroundHeroSection.mp4";
export const SectionHeroVideo = () => {
  return (
    <>
      <video
        className="video-section"
        src={BackgroundHeroSection}
        autoPlay="true"
        muted
        controls
        loop
      />
    </>
  );
};
