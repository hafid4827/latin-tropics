import React from "react";
/* media img */
import Recurse11100 from "../static/img/base_img/Recurse_11_100.jpg";
import Recurse12100 from "../static/img/base_img/Recurse_12_100.jpg";
import Recurse13100 from "../static/img/base_img/Recurse_13_100.jpg";
import Recurse14100 from "../static/img/base_img/Recurse_14_100.jpg";
import Recurse17100 from "../static/img/base_img/Recurse_17_100.jpg";
/* components */
import { BtnGenericBuy } from "./Btn/BtnGenericBuy";
export const SectionDetailBuy = () => {
  return (
    <div className="relative md:h-[100rem] h-auto md:mb-0 mb-52">
      {/* info more */}
      <div className="md:mb-0 mb-16">
        <img src={Recurse17100} alt="" className="w-full h-96" />
      </div>
      <div className="md:w-9/12 w-full mx-auto md:mb-0 mb-16">
        <div className="md:w-3/4 xl:w-1/2 w-full bg-[color:var(--color-coffee-light-1)] px-5 md:h-[46rem] h-96 flex justify-center items-center">
          <p className="text-center text-2xl text-white md:w-1/2 md:-ml-32">
            Linos y twills de algodon en colores moka y terrosos contrastan con
            tonalidades vibrantes como el azafran.
          </p>
        </div>
      </div>
      {/* ----------- */}
      {/* products view img */}
      <div className="md:absolute w-full top-0 left-0 z-50">
        <div className="relative h-auto md:h-[100rem] w-full">
          <div className="md:absolute md:w-96 w-full top-10 left-44 md:mb-0 mb-16">
            <img src={Recurse11100} alt="" className="w-full h-full" />
          </div>
          <div className="md:absolute md:w-96 w-full top-36 xl:right-96 right-36 md:mb-0 mb-16">
            <img src={Recurse12100} alt="" className="w-full h-full" />
          </div>
          <div className="md:absolute md:w-96 w-full bottom-32 left-44 md:mb-0 mb-16">
            <img src={Recurse13100} alt="" className="w-full h-full" />
            <div className="flex justify-center -mt-5">
              <BtnGenericBuy value="comprar ahora" />
            </div>
          </div>
          <div className="md:absolute md:w-[30rem] w-full bottom-10 xl:right-96 right-20 md:mb-0 mb-16">
            <img src={Recurse14100} alt="" className="w-full h-full" />
          </div>
        </div>
      </div>
      {/* ---------- */}
    </div>
  );
};
