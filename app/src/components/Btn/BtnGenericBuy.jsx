import React from "react";

export const BtnGenericBuy = ({ value }) => {
  return (
    <button className="capitalize bg-[color:var(--color-green-medium)] text-white py-2 px-10 font-semibold text-xl hover:scale-105 scale-100 transition-all">
      {value}
    </button>
  );
};
