import React from "react";
/* media img */
import Recurse7100 from "../static/img/base_img/Recurse_7_100.jpg";
import Recurse8100 from "../static/img/base_img/Recurse_8_100.jpg";
import Recurse9100 from "../static/img/base_img/Recurse_9_100.jpg";
/* components */
import { BtnGenericBuy } from "./Btn/BtnGenericBuy";
export const SectionCATBuy = () => {
  return (
    <div className="xl:mt-48 md:mt-5 mt-20 mb-5">
      <div className="md:grid w-[90%] mx-auto md:h-[40rem] grid-cols-3 grid-rows-2 gap-5">
        <div className=" row-span-3">
          <img src={Recurse7100} alt="" className="w-full h-full" />
        </div>
        {/* element decoration paragrap */}
        <div className="md:w-auto h-96 w-full md:h-full bg-[color:var(--color-coffee-medium-1)] flex justify-center items-center">
          <p className="text-center text-[color:var(--color-coffee-force-1)] text-2xl font-semibold">
            Lentes de sol "CAT" en carey, un must para este verano
          </p>
        </div>
        {/* -------- */}
        <div className="row-end-3 col-start-2">
          <img src={Recurse8100} alt="" className="w-full h-[20rem]" />
        </div>
        <div className="row-span-3 col-start-3">
          <img src={Recurse9100} alt="" className="w-full h-full" />
        </div>
      </div>
      <div className="flex justify-center -mt-5">
        <BtnGenericBuy value="comprar ahora" />
      </div>
    </div>
  );
};
