import React from "react";
/* media img */
import Recurse15100 from "../static/img/base_img/Recurse_15_100.jpg";
import Recurse16100 from "../static/img/base_img/Recurse_16_100.jpg";
/* components */
import { BtnGenericBuy } from "./Btn/BtnGenericBuy";
export const SectionFooterBuy = () => {
  return (
    <div className="relative w-full mb-40 xl:mt-52">
      <div className="w-full h-full">
        <img
          src={Recurse16100}
          alt="background image palm trees and arched column"
        />
      </div>
      <div className="absolute top-0 left-0 w-full h-full flex justify-center items-center z-50">
        <div className="">
          <img
            src={Recurse15100}
            alt="girl dressed in a palm tree print blouse"
            className="xl:w-full w-[30rem]"
          />
          <div className="absolute flex justify-center -bottom-14 left-0 w-full ">
            <BtnGenericBuy value="Comprar ahora" />
          </div>
        </div>
      </div>
    </div>
  );
};
