import React from "react";
/* components */
import { BtnGenericBuy } from "./Btn/BtnGenericBuy";
/* media img */
import Recurse2 from "../static/img/base_img/Recurse_2_100.jpg";
export const SectionMaxi = () => {
  return (
    <>
      <div className="relative md:mb-0 mb-24">
        <img className="w-full" src={Recurse2} alt="" />
        <div className="md:w-1/3 w-full static md:absolute right-56 top-20">
          <p className="text-2xl w-full font-semibold text-white bg-[color:var(--color-coffee-medium-1)] md:bg-[color:var(--color-coffee-medium)] px-10 pt-10 pb-14 text-center">
            Maxi estampados inspirados en la flora tropical son los
            protagonistas de la nueva coleccion.
          </p>
          <div className="w-full flex justify-center -mt-6">
            <BtnGenericBuy value="comprar ahora" />
          </div>
        </div>
      </div>
    </>
  );
};
